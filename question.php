<!DOCTYPE html>
<?php session_start(); ?>
<?php include 'database.php'; ?>
<?php 
//номер вопроса хватаем с урла
	$number = (int) $_GET['n'];
//выборка количества вопросов
	$query = "SELECT * FROM questions";
	$results=$mysqli->query($query) or die($mysqli->error._LINE_);
	$total=$results->num_rows;

//выборка вопросов по номеру
	$query = "SELECT * FROM questions WHERE question_number = $number";
	$result = $mysqli->query($query) or die($mysqli->error._LINE_);
	$question = $result->fetch_assoc();

//выборка вариантов
	$query = "SELECT * FROM choices WHERE question_number = $number";
	$choices = $mysqli->query($query) or die($mysqli->error._LINE_);

?>
<html>
	<head>
	<meta charset="utf-8" />
	<title>PHP quiz</title>
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	</head>
<body>
	<header>
 		<div class="container">
 			<h1>PHP quiz</h1>
 		</div>
	</header>
	<main>
		 <div class="container">
		 	<div class="current">QUestion <?php echo $question['question_number'];?> of  <?php echo $total;?></div>
		 	<p class="question">
		 		<?php echo $question['text'] ?>
		 	</p>
		 	<form method="post" action="process.php">
		 		<ul class="choices">
		 			<?php while($row = $choices->fetch_assoc()): ?>
		 					<li><input name="choice" type="radio" value="<?php echo $row['id']; ?>" /><?php echo $row['text']?></li>
		 			<?php endwhile; ?>
		 		</ul>
		 		<input type="submit" value="submit">
		 		<input type="hidden" name="number" value="<?php echo $number; ?>">
		 	</form>
 		</div>
	</main>
	<footer>
		 	<div class="container">
 			copyright, 2015
 		</div>
	</footer>
</body>
</html>