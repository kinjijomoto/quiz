<?php include 'database.php'; ?>

<?php
	//делаем выборку из таблицы 
	$query = "SELECT * FROM questions";
	$results=$mysqli->query($query) or die($mysqli->error._LINE_);
	//количество вопросов(ряды в табле)
	$total = $results->num_rows;
?>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8" />
	<title>PHP quiz</title>
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	</head>
<body>
	<header>
 		<div class="container">
 			<h1>PHP quiz challenge</h1>
 		</div>
	</header>
	<main>
		 	<div class="container">
 			<h2>Testing quizes </h2>
 			<p>This is multiple choice quiz to test your skills</p>
 			<ul>
 				<li><strong>Count: </strong> <?php echo $total; ?> </li>
 				<li><strong>Type: </strong> Multiply choices </li>
 				<li><strong>Time: </strong> <?php echo $total* .5; ?> minutes </li>
 			</ul>
 			<a href="question.php?n=1" class="start">Start</a>
 		</div>
	</main>
	<footer>
		 	<div class="container">
 			copyright, 2015
 		</div>
	</footer>
</body>
</html>